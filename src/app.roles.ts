
export enum AppRoles {
    ADMIN = 'ADMIN',
    AUX = 'AUX',
    CONS = 'CONS',
    VEND = 'VEND'
}
