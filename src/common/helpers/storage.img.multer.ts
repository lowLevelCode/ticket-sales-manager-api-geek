import { diskStorage } from 'multer';
import { extname } from 'path';
import { v4 as uuidv4 } from 'uuid';

export const editFileName = (req, file, callback) => { 
    console.log("file",file);
    const fileExtName = extname(file.originalname);    
    callback(null, `${uuidv4()}${fileExtName}`);
  };
  
export const imageFileFilter = (req, file, callback) => {
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
      return callback(new Error('Only image files are allowed!'), false);
    }
    callback(null, true);
};

export const storageImg = {
    storage: diskStorage({
        destination: './public/img',
        filename: editFileName
    }),
    fileFilter: imageFileFilter,

}