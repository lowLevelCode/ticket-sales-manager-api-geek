import { CreateDateColumn, VersionColumn, PrimaryGeneratedColumn, Column } from "typeorm";

export class BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn()
    createdAt: Date;

    @CreateDateColumn()
    updatedAt: Date;

    @Column({ default: true })
    status: boolean;

    @VersionColumn()
    version:number;
}