import { Entity, Column, OneToMany } from "typeorm";
import { BaseEntity } from "./base.entity";
import { Ventas } from "./ventas.entity";

@Entity()
export class Clientes extends BaseEntity {

    @Column({ type: 'varchar', length: 255, nullable: false })
    nombre: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    apellidos: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    telefono: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    email: string;

    @OneToMany(() => Ventas, venta => venta.cliente)
    ventas: Ventas[];
}