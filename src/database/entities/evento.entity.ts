import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Funciones } from './funciones.entity';
import { TipoEvento } from './tipo-evento.entity';

@Entity()
export class Evento extends BaseEntity{

    @Column()
    nombre:string;

    @OneToMany(() => Funciones, funcion => funcion.evento)
    funciones: Funciones[];
    
    @ManyToOne(()=> TipoEvento, tipoEvento => tipoEvento.eventos)
    tipoEvento:TipoEvento;
}