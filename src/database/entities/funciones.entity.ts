import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, ManyToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Evento } from './evento.entity';
import { Ventas } from './ventas.entity';

@Entity()
export class Funciones extends BaseEntity{

    @Column()
    nombre:string;

    @Column({type:'date'})
    fecha:Date;

    @Column({type:'time'})
    hora:Date;

    @ManyToOne(() => Evento, evento => evento.funciones)
    evento: Evento;

    @ManyToMany(() => Ventas, venta => venta.funciones)
    ventas: Ventas[];
    
}