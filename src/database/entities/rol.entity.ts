import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { BaseEntity } from './base.entity';

@Entity()
export class Rol extends BaseEntity{

    @Column()
    nombre:string;

    @Column({unique:true})
    type:string;
}