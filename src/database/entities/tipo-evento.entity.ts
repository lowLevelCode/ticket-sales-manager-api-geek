import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Evento } from './evento.entity';
import { Funciones } from './funciones.entity';

@Entity()
export class TipoEvento extends BaseEntity {

    @Column()
    nombre:string;

    @OneToMany(() => Evento, funcion => funcion.tipoEvento)
    eventos: Evento[];
}