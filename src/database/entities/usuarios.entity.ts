import { Column, Entity, BeforeInsert, BeforeUpdate, ManyToMany,JoinTable, OneToMany } from "typeorm";
import { BaseEntity } from "./base.entity";
import { hash } from "bcrypt";
import { Rol } from "./rol.entity";
import { Ventas } from "./ventas.entity";

@Entity()
export class Usuarios extends BaseEntity {
    @Column({ type: 'varchar', length: 255, nullable: false })
    nombre: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    apellidos: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    email: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    password: string;

    @ManyToMany(type => Rol)
    @JoinTable()
    roles: Rol[]

    @OneToMany(() => Ventas, venta => venta.cliente)
    ventas: Ventas[];

    @BeforeInsert()
    @BeforeUpdate()
    async hashPassword() {  
      if (!this.password)  return; 
      this.password = await hash(this.password, 10);
    }
}