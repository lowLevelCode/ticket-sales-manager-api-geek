import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Clientes } from './clientes.entity';
import { Funciones } from './funciones.entity';
import { TipoEvento } from './tipo-evento.entity';
import { Usuarios } from './usuarios.entity';

@Entity()
export class Ventas extends BaseEntity{    
    
    @ManyToOne(()=> Clientes, tipoEvento => tipoEvento.ventas)
    cliente:Clientes;

    @ManyToOne(()=> Usuarios, usuario => usuario.ventas)
    usuario:Usuarios;

    @ManyToMany(() => Funciones, funcion => funcion.ventas)
    @JoinTable()
    funciones: Funciones[];
}