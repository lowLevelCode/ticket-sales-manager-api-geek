import { BadRequestException } from "@nestjs/common";
import { IPaginationOptions, paginate, Pagination } from "nestjs-typeorm-paginate";
import { FilterKeyDto } from "src/modules/shared/dtos/filter.dto";
import { Brackets, EntityRepository, Repository } from "typeorm";
import { Clientes } from "../entities/clientes.entity";

@EntityRepository(Clientes)
export class ClientesRepository extends Repository<Clientes> {

    private getQueryBuilder() {
        return this.createQueryBuilder("c")
        .select("c.id")
        .addSelect("c.nombre")
        .addSelect("c.apellidos")
        .addSelect("c.telefono")
        .addSelect("c.email");        
    }
    
    async paginateFilterByKey(options: IPaginationOptions, filter:FilterKeyDto): Promise<Pagination<Clientes>> {        
        const {keyword} = filter;        
        if(!keyword || keyword.trim()==='')
            throw new BadRequestException("La palabra clave no debe de ir vacia.");    

        const query = this.getQueryBuilder()
        .where(new Brackets(qb=>{
            qb.where("c.nombre like :nombre", { nombre:`%${keyword}%` })
            .orWhere("c.apellidos like :apellidos", { apellidos:`%${keyword}%` })
            .orWhere("c.telefono like :telefono", { telefono:`%${keyword}%` })
            .orWhere("c.email like :email", { email:`%${keyword}%` })
        }))
        .andWhere("c.status = :status",{ status:true });

        return paginate<Clientes>(query,options);;        
    }
}