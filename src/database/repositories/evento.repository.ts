import { BadRequestException } from "@nestjs/common";
import { IPaginationOptions, paginate, Pagination } from "nestjs-typeorm-paginate";
import { FilterById, FilterByKeyandId, FilterKeyDto } from "src/modules/shared/dtos/filter.dto";
import {EntityRepository, Repository} from "typeorm";
import { Evento } from "../entities/evento.entity";

@EntityRepository(Evento)
export class EventoRepository extends Repository<Evento> {

    private getQueryBuilder(){
        return this.createQueryBuilder("e")
        .select("e.id")
        .addSelect("e.nombre")
        .addSelect("tipo.id")
        .addSelect("tipo.nombre")
        .addSelect("funciones.id")
        .addSelect("funciones.nombre")
        .addSelect("funciones.fecha")
        .addSelect("funciones.hora")
        .leftJoin("e.tipoEvento","tipo")
        .leftJoin("e.funciones", "funciones");
    }

    async paginate(options: IPaginationOptions): Promise<Pagination<Evento>> {
        const queryBuilderScript = this.getQueryBuilder();
        return paginate<Evento>(queryBuilderScript,options);
    }

    async paginateFilterByKey(options: IPaginationOptions, filter:FilterKeyDto): Promise<Pagination<Evento>> {        
        
        const {keyword} = filter; 
        if(!keyword || keyword.trim()==='')
            throw new BadRequestException("La palabra clave no debe de ir vacia.");    
        
        let keyLower = keyword.toLowerCase();
        
        const evento = this.getQueryBuilder();

        evento.where("LOWER(e.nombre) like :nombre", { nombre:`%${keyLower}%` })
        .orWhere("LOWER(tipo.nombre)  like :nombre", { nombre:`%${keyLower}%` });


        return paginate<Evento>(evento,options);
    }

    async paginateFilterByTipoEvento(options: IPaginationOptions, filter:FilterById): Promise<Pagination<Evento>> {        
        
        const {id} = filter;
        const evento = this.getQueryBuilder();
        evento.where("tipo.id = :id", {id})
        return paginate<Evento>(evento,options);
    }

    async paginateFilterByKeyNTipoEvento(options: IPaginationOptions, filter:FilterByKeyandId): Promise<Pagination<Evento>> {        
        
        const {keyword, id} = filter;
        const keyLower = keyword.toLowerCase();

        if(!keyword || keyword.trim()==='')
            throw new BadRequestException("La palabra clave no debe de ir vacia.");
        
        if(!id || id.trim()==='')
            throw new BadRequestException("El rol no debe de ser vacio.")

        const evento = this.getQueryBuilder();
        evento.where("LOWER(e.nombre) like :nombre", { nombre:`%${keyLower}%` })
        .orWhere("LOWER(tipo.nombre)  like :nombre", { nombre:`%${keyLower}%` })
        .orWhere("tipo.id = :id", {id});

        return paginate(evento,options);
    }
}