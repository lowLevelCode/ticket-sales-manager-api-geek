import { BadRequestException } from "@nestjs/common";
import { IPaginationOptions } from "nestjs-typeorm-paginate/dist/interfaces";
import { paginate } from "nestjs-typeorm-paginate/dist/paginate";
import { Pagination } from "nestjs-typeorm-paginate/dist/pagination";
import { FilterKeyDto, FilterRolDto } from "src/modules/shared/dtos/filter.dto";
import {Brackets, EntityRepository, Like, Repository} from "typeorm";
import { Usuarios } from "../entities/usuarios.entity";

@EntityRepository(Usuarios)
export class UsuarioRepository extends Repository<Usuarios> {

    private getQueryBuilder(){
        return this.createQueryBuilder("user")
        .select("user.id")
        .addSelect("user.nombre")
        .addSelect("user.apellidos")
        .addSelect("user.email")
        .addSelect("rol.id")
        .addSelect("rol.id")
        .addSelect("rol.type")
        .leftJoin("user.roles", "rol");
    }

    async paginate(options: IPaginationOptions): Promise<Pagination<Usuarios>> {
        const queryBuilder = this.createQueryBuilder("u")
        .leftJoinAndSelect("u.roles","rol")
        .where("u.status = :status",{status:true});

        return paginate<Usuarios>(queryBuilder,options);
    }

    async paginateFilterByKeyNRol(options: IPaginationOptions, filter:any): Promise<Pagination<Usuarios>> {
        const keyword = filter.keyword;
        const rol = filter.rol;

        if(!keyword || keyword.trim()==='')
            throw new BadRequestException("La palabra clave no debe de ir vacia.");
        
        if(!rol || rol.trim()==='')
            throw new BadRequestException("El rol no debe de ser vacio.")

        const user = this.getQueryBuilder();
        user.where(new Brackets(qb=>{
            qb.where("user.nombre like :nombre", { nombre:`%${keyword}%` })
            .orWhere("user.apellidos like :apellidos", { apellidos:`%${keyword}%` })
            .orWhere("user.email like :email", { email:`%${keyword}%` })
            .orWhere("rol.type = :type", { type:rol })
        }))
        .andWhere("user.status = :status",{ status:true });        

        return paginate<Usuarios>(user,options);
    }

    async paginateFilterByKey(options: IPaginationOptions, filter:FilterKeyDto): Promise<Pagination<Usuarios>> {        
        const {keyword} = filter;        
        if(!keyword || keyword.trim()==='')
            throw new BadRequestException("La palabra clave no debe de ir vacia.");    

        const user = this.getQueryBuilder();
        user.where(new Brackets(qb=>{
            qb.where("user.nombre like :nombre", { nombre:`%${keyword}%` })
            .orWhere("user.apellidos like :apellidos", { apellidos:`%${keyword}%` })
            .orWhere("user.email like :email", { email:`%${keyword}%` })
        }))
        .andWhere("user.status = :status",{ status:true });        

        return paginate<Usuarios>(user,options);
    }

    async paginateFilterByRol(options: IPaginationOptions, filter:FilterRolDto): Promise<Pagination<Usuarios>> {        
        const {rol} = filter;        
        if(!rol || rol.trim()==='')
            throw new BadRequestException("El rol no debe de ser vacio.")
            
        const user = this.getQueryBuilder();
        user.where("rol.type = :type", { type:rol })
        .andWhere("user.status = :status",{ status:true });

        return paginate<Usuarios>(user,options);
    }
}