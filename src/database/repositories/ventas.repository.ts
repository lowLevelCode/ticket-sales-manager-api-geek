import { EntityRepository, Repository } from "typeorm";
import { Ventas } from "../entities/ventas.entity";

@EntityRepository(Ventas)
export class VentasRepository extends Repository<Ventas> {}