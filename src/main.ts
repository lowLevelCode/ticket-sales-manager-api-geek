import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config/dist';
import { NestFactory } from '@nestjs/core';
import { initSwagger } from 'src/app.swagger';
import { AppModule } from './app.module';
import { SERVER_PORT } from './config/config.env';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const logger = new Logger('Init Bootstrap');
  const config = app.get(ConfigService);
  const port = parseInt(config.get<string>(SERVER_PORT), 10) || 3000;

  
  initSwagger(app);

  app.setGlobalPrefix('api');
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  );
  
  
  await app.listen(port);
  logger.log(`Server is running at ${await app.getUrl()}`);
}
bootstrap();
