import { Controller, Get, Query, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { FilterKeyDto } from '../shared/dtos/filter.dto';
import { PaginationDto } from '../shared/dtos/pagination.dto';
import { ClientesService } from './clientes.service';
import { CreateClienteDto } from './dtos/create-cliente.dto';
import { UpdateClienteDto } from './dtos/update-cliente.dto';

@Controller('clientes')
export class ClientesController {

    constructor(private readonly _clienteService:ClientesService){}
    
    @Get('filter-by-key')
    async getPaginateUsuariosFilterKey(@Query() paginationDto:PaginationDto, @Query() filter:FilterKeyDto) {        
        return this._clienteService.getPaginateUsuariosFilterByKey(paginationDto,filter);
    }

    @Post('register')
    register(@Body() clienteDto:CreateClienteDto) {        
        return this._clienteService.register(clienteDto);        
    }

    @Put('update/:id')
    async updateOne(@Param('id') id:string | number, @Body() clienteDto:UpdateClienteDto){
        return this._clienteService.updateOne(id,clienteDto);
    }

    @Delete('delete/:id')
    async deleteOne(@Param('id') id:string | number){
        return this._clienteService.deleteOne(id);
    }
}
