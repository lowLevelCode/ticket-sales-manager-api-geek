import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientesRepository } from 'src/database/repositories/clientes.repository';
import { ClientesController } from './clientes.controller';
import { ClientesService } from './clientes.service';

@Module({
  imports:[TypeOrmModule.forFeature([ClientesRepository])],
  controllers: [ClientesController],
  providers: [ClientesService]
})
export class ClientesModule {}
