import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { IPaginationOptions } from 'nestjs-typeorm-paginate';
import { Clientes } from 'src/database/entities/clientes.entity';
import { ClientesRepository } from 'src/database/repositories/clientes.repository';
import { FilterKeyDto } from '../shared/dtos/filter.dto';
import { CreateClienteDto } from './dtos/create-cliente.dto';
import { UpdateClienteDto } from './dtos/update-cliente.dto';

@Injectable()
export class ClientesService {
    
    constructor(private readonly _clientesRepository:ClientesRepository){}

    async getPaginateUsuariosFilterByKey(options:IPaginationOptions, filter:FilterKeyDto) {
        return this._clientesRepository.paginateFilterByKey(options,filter);
    }

    async register(clienteDto:CreateClienteDto) {
        const clienteExist = await this._clientesRepository.findOne({ email: clienteDto.email });
        if (clienteExist)
            throw new BadRequestException('Client already registered with email');
        
        const newCliente = this._clientesRepository.create(clienteDto);
        return await this._clientesRepository.save(newCliente);        
    }

    async updateOne(id:string | number, clienteDto:UpdateClienteDto){
        if(!id)
            throw new BadRequestException('id is empty, null or undefined');

        const userEmailExist = await this._clientesRepository.findOne({ email: clienteDto.email });
        if (userEmailExist && userEmailExist.id != id)
            throw new BadRequestException('User already registered with email');

        let clienteExist:Clientes = await this._clientesRepository.findOne(id); 
        if(!clienteExist || !clienteExist.status) 
            throw new NotFoundException();

        Object.assign(clienteExist, clienteDto);

        return await this._clientesRepository.save(clienteExist);
    }

    async deleteOne(id:string | number) {
        if(!id)
            throw new BadRequestException('id is empty, null or undefined');

        let clienteExist:Clientes = await this._clientesRepository.findOne(id); 

        if(!clienteExist || !clienteExist.status)
            throw new NotFoundException();
        
        clienteExist.status = false;
        await this._clientesRepository.save(clienteExist);

        return `Cliente ${id} eliminado correctamente`;
    }
}
