import { IsEmail, IsNotEmpty, IsPhoneNumber } from "class-validator";

export class CreateClienteDto {
    
    @IsNotEmpty()
    nombre: string;
    
    @IsNotEmpty()
    apellidos: string;
    
    @IsNotEmpty()
    @IsPhoneNumber("any")
    telefono: string;
    
    @IsNotEmpty()
    @IsEmail()    
    email: string;
}