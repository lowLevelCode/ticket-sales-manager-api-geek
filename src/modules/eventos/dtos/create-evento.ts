import { ApiProperty } from "@nestjs/swagger";
import { ArrayNotEmpty, IsArray, IsNotEmpty, IsString, IsUUID } from "class-validator";


export const eventosCulturalesExample = [
    'Festival Internacional de las Luces',
    'Festival Internacional de cine de Morelia',
    'Feria Internacional del Libro en Guadalajara',
    'Equinoccio de Primavera en Chichen Itzá',
    'Festival Internacional de Tamaulipas',
];

export const tiposEventosCulturalesExample = [
    'Inauguraciones de centros culturales',
    'Exposiciones de obras artísticas',
    'Festivales o fiestas culturales',
    'Espectáculos callejeros',
    'Cursos artísticos',
]

export class FuncionesDto{
    @IsNotEmpty()
    nombre:string;

    @IsNotEmpty()
    fecha:Date;

    @IsNotEmpty()
    hora:Date;
}

export class CreateEventoDto {
    @ApiProperty({description:"Nombre de del evento", type:String, examples:eventosCulturalesExample})
    @IsNotEmpty()
    nombre:string;
    
    @ApiProperty({description:"Funciones que pertenecen al evento"})
    @ArrayNotEmpty()
    @IsArray()
    funciones:FuncionesDto[];

    @IsUUID()
    tipoEvento:string;

    imgPath:string;
}