import { Controller, Post, Body, Query, Get, Param, Put, Delete, UseInterceptors, UploadedFiles, UploadedFile } from '@nestjs/common';
import { EventosService } from './eventos.service';
import { UpdateEventoDto } from './dtos/update-evento';
import { FileInterceptor } from '@nestjs/platform-express/multer/interceptors/file.interceptor';
import { storageImg } from 'src/common/helpers/storage.img.multer';
import { CreateEventoDto } from './dtos/create-evento';
import { PaginationDto } from '../shared/dtos/pagination.dto';
import { FilterById, FilterByKeyandId, FilterKeyDto } from '../shared/dtos/filter.dto';


@Controller('eventos')
export class EventosController {

    constructor(private readonly _eventoService:EventosService){}

    @Get()
    async getPaginateEventos(@Query() paginationDto:PaginationDto){
        return this._eventoService.getPaginateEventos(paginationDto);
    }

    @Get('filter-by-key')
    async getPaginateEventosFilterKey(@Query() paginationDto:PaginationDto, @Query() filter:FilterKeyDto){
        return this._eventoService.getPaginateEventosFilterKey(paginationDto, filter);
    }

    @Get('filter-by-tipo-evento')
    async getPaginateUsuariosFilterRol(@Query() paginationDto:PaginationDto, @Query() filter:FilterById) {        
        return this._eventoService.getPaginateEventoFilterByTipoEvento(paginationDto,filter);
    }

    @Get('filter-by-key-tipo')
    async getPaginateUsuariosFilterKeyNTipoEvento(@Query() paginationDto:PaginationDto, @Query() filter:FilterByKeyandId) {             
        return this._eventoService.getPaginateUsuariosFilterKeyNTipoEvento(paginationDto,filter);
    }

    @Post('register')
    //@UseInterceptors(FileInterceptor('image', storageImg))
    register(@Body() eventoDto:CreateEventoDto) {
        // const {path} = img;(@UploadedFile() img:any,
        eventoDto.imgPath = "path"; 

        return this._eventoService.register(eventoDto);
    }

    @Put('update/:id')
    async updateOne(@Param('id') id:string | number, @Body() eventoDto:UpdateEventoDto){
        return this._eventoService.updateOne(id,eventoDto);
    }

    @Delete('delete/:id')
    async deleteOne(@Param('id)') id:string | number){
        return this._eventoService.deleteOne(id);
    }
}
