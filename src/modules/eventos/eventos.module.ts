import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventoRepository } from 'src/database/repositories/evento.repository';
import { EventosController } from './eventos.controller';
import { EventosService } from './eventos.service';

@Module({
  imports:[TypeOrmModule.forFeature([EventoRepository])],
  controllers: [EventosController],
  providers: [EventosService]
})
export class EventosModule {}
