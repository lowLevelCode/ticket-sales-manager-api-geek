import { BadRequestException, Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { Evento } from 'src/database/entities/evento.entity';
import { Funciones } from 'src/database/entities/funciones.entity';
import { EventoRepository } from 'src/database/repositories/evento.repository';
import { getConnection } from 'typeorm';
import { CreateEventoDto, FuncionesDto } from './dtos/create-evento';
import { UpdateEventoDto } from './dtos/update-evento';
import * as moment from 'moment';
import { IPaginationOptions } from 'nestjs-typeorm-paginate';
import { FilterById, FilterByKeyandId, FilterKeyDto } from '../shared/dtos/filter.dto';

@Injectable()
export class EventosService {
    
    constructor(private readonly _eventoRepo:EventoRepository){}

    async getPaginateEventos(options:IPaginationOptions){
        return this._eventoRepo.paginate(options);        
    }

    async getPaginateEventosFilterKey(options:IPaginationOptions, filter:FilterKeyDto){
        return this._eventoRepo.paginateFilterByKey(options,filter);        
    }

    async getPaginateEventoFilterByTipoEvento(options:IPaginationOptions, filter:FilterById){
        return this._eventoRepo.paginateFilterByTipoEvento(options,filter);        
    }    

    async getPaginateFilterByKeyNTipoEvento(options:IPaginationOptions, filter:FilterById){
        return this._eventoRepo.paginateFilterByTipoEvento(options,filter);        
    }        

    async getPaginateUsuariosFilterKeyNTipoEvento(options:IPaginationOptions, filter:FilterByKeyandId){
        return this._eventoRepo.paginateFilterByKeyNTipoEvento(options,filter);        
    }        
    
    async register(eventoDto:CreateEventoDto) {

        const {nombre} = eventoDto;
        const {tipoEvento} = eventoDto;

        const nombreTrim = nombre.trim();
        const tipoEventoTrim = tipoEvento.trim();
        
        // No se debe de repetir eventos con el mismo nombre y tipo de evento
        await this.validarEventosRepetidos(nombreTrim,tipoEventoTrim);

        // No debe de haber funciones con la misma fecha.
        let funciones:Funciones[] = this.validarFechaFunciones(eventoDto.funciones);        

        // Start transaction
        const connection = getConnection();
        const queryRunner = connection.createQueryRunner();         

        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            // Guardamos las funciones                        
            const savedFunciones = await queryRunner.manager.save(funciones);
            delete eventoDto.funciones; // solo hacemos delete de los elementos del dto. * TODO: esta accion no es necesaria.

            // Guardamos el evento
            let evento:Evento = new Evento();
            Object.assign(evento, eventoDto);
            evento.funciones = savedFunciones;

            const eventoSaved = await queryRunner.manager.save(evento);

            await queryRunner.commitTransaction();  // si todo sale bien hacemos commit tal cual como con una stored procedure

            return eventoSaved; // al final retornamos el evento guardaddo.
        } catch (error) {            
            await queryRunner.rollbackTransaction();    // rollback en caso de un error interno
            throw new InternalServerErrorException("ROLLBACK");   // retornamos un error interno hacia el cliente.
        } finally {
            await queryRunner.release();    // liberamos la memoria usada para la transaccion incluso la conexion.
        }
    }

    // TODO: terminar este metodo... necesitamos el modulo de ventas.
    async updateOne(id:string | number, eventoDto:UpdateEventoDto) {
        // clasulas de guardia.
        if(!id)
            throw new BadRequestException('id is empty, null or undefined');
        
        const {nombre} = eventoDto;
        const {tipoEvento} = eventoDto;
        const {funciones} = eventoDto;

        const nombreTrim = nombre.trim();
        const tipoEventoTrim = tipoEvento.trim();

        // No se debe de repetir eventos con el mismo nombre y tipo de evento
        await this.validarEventosRepetidos(nombreTrim,tipoEventoTrim, id);        


        // verificamos que el efectivamente exista un elemento a modificar.
        // hacemos la consulta correspondiente. Para encontrar el evento a modificar
        let eventoExist:Evento = await this._eventoRepo.findOne({
            relations:["funciones"],
            where:{ id }
        });

        if(!eventoExist) 
            throw new NotFoundException(`El evento con id '${id}' no existe.`); // lanzamos un 404

        // Start transaction
        const connection = getConnection();
        const queryRunner = connection.createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
            // creamos el repositorio de las funciones
            const funcionesRepo = queryRunner.manager
            .getRepository(Funciones);

            // obtenemos las funciones vendidas hacemos filter.
            // TODO: hace filtro de funciones vendidas con .filter() -> donde las ventas sean mayores a 0
            let funcionesVendidas:any[] = [];

            // eliminamos las funciones del evento previamente buscado, con excepcion de las funciones vendidas
            await funcionesRepo.remove(eventoExist.funciones);

            // unimos las funciones vendidas con las nuevas funciones para validar que las fechas esten correctas.
            let validarFechasArray = [...funcionesVendidas, funciones];
            // esto retorna un arreglo... pero en caso de haber un problema con las fechas lanza un bad request
            this.validarFechaFunciones(validarFechasArray); 

            
            
        } catch (error) {
            console.error(error.message);
            await queryRunner.rollbackTransaction();               // rollback en caso de un error interno
            throw new InternalServerErrorException("ROLLBACK");   // retornamos un error interno hacia el cliente.
        } finally {
            await queryRunner.release();    // liberamos la memoria usada para la transaccion incluso la conexion.
        }

        return "evento actualizado";
    }

    // TODO: Terminar este metodo... falta el metodo de ventas.
    async deleteOne(id:string | number){
        // No debe eliminarse un evento con boletos vendidos en funciones.
        return "evento eliminado";
    }

    /**
     * Valida que no haya eventos con el mismo nombre y tipo de evento
     */
    private async  validarEventosRepetidos(nombre?:string, idTipoEvento?:string,id?:string | number) {        
        const existEvento = await this._eventoRepo.createQueryBuilder("e") // se busca un evento con un cierto nombre y tipo de evento.
        .select("e.id")
        .addSelect("e.nombre")
        .addSelect("tipo.nombre")
        .where("e.nombre = :nombre", {nombre:nombre})
        .andWhere("tipo.id = :id", {id:idTipoEvento})
        .leftJoin("e.tipoEvento","tipo")
        .getOne();
            
        if(existEvento) {
            // si encuentra un elemento con con id similar al pasado por parametros retorna sin hacer nada.
            // Esto por queremos modificar ese mismo elemento.        
            if(existEvento && existEvento.id === (id || ''))
                return; // retorna simplemente            

            throw new BadRequestException("No se debe de repetir eventos con el mismo nombre y tipo de evento.");
        }
    }

    /**
     * Se valida que no haya funciones con la misma fecha..
     * @param funcionesIn Arreglo de funciones a evaluar
     * @throws Bad Request - Las fechas de las funciones deben de ser mayor a la fecha actual.
     * @throws Bad Request - No debe de haber funciones con la misma fecha.
     */
    private validarFechaFunciones(funcionesIn:any[]):Funciones[]{
        let funciones:Funciones[] = []; 
        funcionesIn.forEach(element => { // ciclamos las funciones

            // la fecha de las funciones debe de ser mayor a la actual.            
            const isFechaMenorQueActual = moment(element.fecha).isBefore(moment());
            if(isFechaMenorQueActual){
                throw new BadRequestException("Las fechas de las funciones deben de ser mayor a la fecha actual.");
            }

            let funcion = new Funciones();  // creamos una entidad o modelos de las funciones
            Object.assign(funcion,element); // replicamos el valor hacia esa funcion mediante las propiedades de clave valor.

            const existFecha = funciones.filter(f=> f.fecha === funcion.fecha); // buscamos todas la fechas ya tratadas en la funciones para ver si hay repetidas
            if(existFecha.length > 0) { // mandamos un bad request en caso de que se repita la fecha agregar.
                throw new BadRequestException("No debe de haber funciones con la misma fecha.");
            }

            funciones.push(funcion); 
        });

        return funciones;
    }
}
