import { AppRoles } from "src/app.roles";
import { EnumToString } from "src/common/helpers/enumToString";
import { IsNotEmpty, IsArray, IsEnum, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CreateRolDto {
    
    @ApiProperty({description:"Nombre del rol a crear", type:String, example:"Auxiliar"})
    @IsNotEmpty()
    @IsString()
    nombre:string;

    @ApiProperty({description:"Tipo de rol", type:String, enum:EnumToString(AppRoles), example:"AUX"})
    @IsNotEmpty()    
    @IsEnum(AppRoles, {    message: `must be a valid role value, ${EnumToString(AppRoles)}`})
    type: string;
}