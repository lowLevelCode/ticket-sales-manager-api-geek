import { ApiProperty, PartialType } from "@nestjs/swagger";
import { BaseResponseDto } from "src/modules/shared/dtos/base.response.dto";

export class RolResponseDto extends PartialType(BaseResponseDto){    

    @ApiProperty({type:String})
    nombre:string;

    @ApiProperty({type:String})
    type:string;
}