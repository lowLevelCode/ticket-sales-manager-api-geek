import { PartialType } from "@nestjs/swagger";
import { CreateRolDto } from "./create-rol";

export class UpdateRolDto extends PartialType(CreateRolDto){}