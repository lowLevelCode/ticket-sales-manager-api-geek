import { Body, Controller, Delete, Get, Param, Post, Put, Query } from '@nestjs/common';
import { CreateRolDto } from './dtos/create-rol';
import { UpdateRolDto } from './dtos/update-rol';
import { RolService } from './rol.service';
import { ApiTags, ApiOperation, ApiBody, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse, ApiParam, ApiBadRequestResponse, ApiNotFoundResponse } from '@nestjs/swagger';
import { InternalServerRequestResponseNest, BadRequestResponseNest, NotFoundRequestResponseNest } from '../shared/dtos/response-message/response.nest';
import { RolResponseDto } from './dtos/rol.response.dto';

@ApiTags('rol')
@Controller('rol')
export class RolController {

    constructor(private readonly _rolService:RolService){}

    @ApiOperation({description:"Retorna todos los roles activos existentes.", summary:"Retorna los roles."})
    @ApiOkResponse({description:"Retorna un array de roles", type:[RolResponseDto]})
    @ApiInternalServerErrorResponse({description:"Cualquier error interno del servidor", type:InternalServerRequestResponseNest})
    @Get()
    async getRoles() {
        return this._rolService.getRoles();
    }

    @ApiOperation({description:"Registro para un nuevo Rol.", summary:"Registra un nuevo rol."})
    @ApiBody({description:"Recibe un dto para crear un rol", type:CreateRolDto})
    @ApiCreatedResponse({description:"Retorna el usuario registrado",type:RolResponseDto})
    @ApiInternalServerErrorResponse({description:"Cualquier error interno del servidor", type:InternalServerRequestResponseNest})
    @Post('add')
    register(@Body() rolDto:CreateRolDto) {        
        return this._rolService.register(rolDto);
    }

    @ApiOperation({description:"Actualiza Rol por Id.", summary:"Actualiza un rol por el id."})
    @ApiParam({name:'id', type:String, description:'uuid de rol a actualizar'})
    @ApiBody({description:"Recibe un dto para actualizar un rol por id.", type:UpdateRolDto})
    @ApiOkResponse({description:"Retorna el usuario registrado",type:RolResponseDto})
    @ApiBadRequestResponse({description:"Si el id esta vacio, nulo o indefinido.", type:BadRequestResponseNest})
    @ApiNotFoundResponse({description:"Si no existe rol con el id a buscar o si es status = false", type:NotFoundRequestResponseNest})
    @ApiInternalServerErrorResponse({description:"Cualquier error interno del servidor", type:InternalServerRequestResponseNest})
    @Put('update/:id')
    async updateOne(@Param('id') id:string | number, @Body() rolDto:UpdateRolDto) {        
        return this._rolService.updateOne(id,rolDto);
    }

    @ApiOperation({description:"Elimina Rol por Id.", summary:"Elimina un rol por el id."})
    @ApiParam({name:'id', type:String, description:'uuid de rol a eliminar'})    
    @ApiOkResponse({description:"Retorna el mensaje: 'Rol {id} eliminado correctamente.'"})
    @ApiBadRequestResponse({description:"Si el id esta vacio, nulo o indefinido.", type:BadRequestResponseNest})
    @ApiNotFoundResponse({description:"Si no existe rol con el id a buscar o si es status = false", type:NotFoundRequestResponseNest})
    @ApiInternalServerErrorResponse({description:"Cualquier error interno del servidor", type:InternalServerRequestResponseNest})
    @Delete('delete/:id')
    async deleteOne(@Param('id') id:string | number){            
        return this._rolService.deleteOne(id);
    }
}
