import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { RolRepository } from 'src/database/repositories/rol.repository ';
import { CreateRolDto } from './dtos/create-rol';
import { UpdateRolDto } from './dtos/update-rol';
import { Rol } from 'src/database/entities/rol.entity';

@Injectable()
export class RolService {

    constructor(private readonly _rolRepo:RolRepository){}
    
    async getRoles(){
        return this._rolRepo.find({where:{status:true}});
    }

    async register(rolDto:CreateRolDto) {
        return await this._rolRepo.save(rolDto);
    }

    async updateOne(id:string | number, rolDto:UpdateRolDto){
        if(!id)
            throw new BadRequestException('id is empty, null or undefined');
            
        let rolExist:Rol = await this._rolRepo.findOne(id); 
        if(!rolExist) 
            throw new NotFoundException();

        Object.assign(rolExist, rolDto);   
        
        return await this._rolRepo.save(rolExist);
    }

    async deleteOne(id:string | number){
        if(!id)
            throw new BadRequestException('id is empty, null or undefined');
        let rolExist:Rol = await this._rolRepo.findOne(id); 
        if(!rolExist) 
            throw new NotFoundException();
        
        rolExist.status = false;
        await this._rolRepo.save(rolExist);

        return "Rol " + id + " eliminado correctamente.";
    }
}
