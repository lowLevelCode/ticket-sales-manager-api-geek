import { ApiProperty } from "@nestjs/swagger";

export class BaseResponseDto {
    @ApiProperty({type:String})
    id:string;

    @ApiProperty({type:Date})
    createdAt:Date;

    @ApiProperty({type:Date})
    updatedAt: Date;
    
    @ApiProperty({type:Boolean})
    status: boolean;
    
    @ApiProperty({type:Number})
    version: number;
}