import { ApiOperation, ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty, IsNumberString, IsString, IsUUID } from "class-validator";
import { AppRoles } from "src/app.roles";
import { EnumToString } from "src/common/helpers/enumToString";

export class FilterKeyRolDto {
    
    @ApiProperty({description:"Palabra clave para filtrar",type:String})
    @IsNotEmpty()
    @IsString()
    keyword:string; 

    @ApiProperty({description:"Rol con el cual filtrar", type:String, enum: EnumToString(AppRoles)})    
    @IsEnum(AppRoles, {        
        message: `must be a valid role value, ${EnumToString(AppRoles)}`,
    })
    rol: string;
}

export class FilterRolDto{
    @ApiProperty({description:"Rol con el cual filtrar", type:String, enum: EnumToString(AppRoles)})    
    @IsEnum(AppRoles, {        
        message: `must be a valid role value, ${EnumToString(AppRoles)}`,
    })
    rol: string;
}

export class FilterKeyDto {

    @ApiProperty({description:"Palabra clave para filtrar",type:String})
    @IsNotEmpty()
    @IsString()
    keyword:string; 
}

export class FilterById{

    @IsUUID()
    id:string;
}

export class FilterByKeyandId {

    @IsNotEmpty()
    @IsString()
    keyword:string; 

    @IsUUID()
    id:string;
}