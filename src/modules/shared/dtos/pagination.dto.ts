import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsNumberString, Max, Min } from "class-validator";

export class PaginationDto {

    @ApiProperty({description:"Indica el numero de pagina a mostrar", default:1, required:false})
    @IsNotEmpty()    
    page:number=1;

    @ApiProperty({description:"Indica el numero de de elementos a mostrar por pagina", default:10, required:false})
    @IsNotEmpty()    
    limit:number=10;

}