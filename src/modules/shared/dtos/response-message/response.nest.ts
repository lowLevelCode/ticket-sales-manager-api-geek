import { ApiProperty, OmitType } from "@nestjs/swagger";

/**
 * Clase base para documentar el tipo de respuesta que da nest.
 * Clase de tipo doc schema.
 * @example {"statusCode": 200,"message": "any message", "error": "Ok" }
 */
class ResponseNest {
    @ApiProperty({description:"Codigo de respuesta http", type:Number, default:0})
    statusCode:number;

    @ApiProperty({description:"Mensaje de respuesta http", type:String, default:"Cualquier mensaje de respuesta."})
    message:string;

    @ApiProperty({description:"Mesaje de error, de la respuesta http", type:String, default:"Cualquier mensaje de error."})
    error:string;
}

/***
 * Clase de tipo doc schema.
 * Sirve para dar un mensaje claro del tipo de respuesta.
 * @example {"statusCode": 400,"message": "any message", "error": "Bad Request" }
 * 
 * Hacemos una herencia de tipo omit type, para poder heredar las propiedades del swagger,
 * y a la vez poder sobrescribir las mismas.
 */
export class BadRequestResponseNest extends OmitType(ResponseNest,['statusCode','error'] as const){
    @ApiProperty({type:Number, default:400})
    statusCode:number;

    @ApiProperty({type:String, default:"Bad Request"})
    error:string;
}

/***
 * Clase de tipo doc schema.
 * Sirve para dar un mensaje claro del tipo de respuesta.
 * @example {"statusCode": 404,"message": "any message", "error": "Not Found" }
 * 
 * Hacemos una herencia de tipo omit type, para poder heredar las propiedades del swagger,
 * y a la vez poder sobrescribir las mismas.
 */
export class NotFoundRequestResponseNest extends OmitType(ResponseNest,['statusCode','error'] as const){
    @ApiProperty({type:Number, default:404})
    statusCode:number;

    @ApiProperty({type:String, default:"Not Found"})
    error:string;
}

/***
 * Clase de tipo doc schema.
 * Sirve para dar un mensaje claro del tipo de respuesta.
 * @example {"statusCode": 500,"message": "any message", "error": "Internal Server" }
 * 
 * Hacemos una herencia de tipo omit type, para poder heredar las propiedades del swagger,
 * y a la vez poder sobrescribir las mismas.
 */
export class InternalServerRequestResponseNest extends OmitType(ResponseNest,['statusCode','error'] as const){
    @ApiProperty({type:Number, default:500})
    statusCode:number;

    @ApiProperty({type:String, default:"Internal Server"})
    error:string;
}