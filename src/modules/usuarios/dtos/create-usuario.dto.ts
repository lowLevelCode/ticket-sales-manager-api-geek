import { ApiProperty } from "@nestjs/swagger";
import { 
    IsOptional, IsString, MaxLength,
    IsEmail, MinLength, IsArray, IsEnum, IsNotEmpty } from "class-validator";

export interface Rol {
    id:any;
}

export class CreateUsuarioDto {

    @ApiProperty({description:"Nombre del usuario",example:"juanito"})
    @IsNotEmpty()
    @IsString()
    @MaxLength(255)
    nombre: string;

    @ApiProperty({description:"Apellidos del usuario",example:"perez"})
    @IsNotEmpty()
    @IsString()
    @MaxLength(255)
    apellidos: string;

    @ApiProperty({description:"Correo del usuario",example:"example@gmail.com"})
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @ApiProperty({description:"Contraseña del usuario",example:"cualquierContraseña"})
    @IsNotEmpty()
    @IsString()
    @MinLength(8)
    @MaxLength(128)
    password: string;

    @ApiProperty({description:"Roles del usuario",example:[{id:'uuid'}]})
    @IsNotEmpty()
    @IsArray()
    roles:Rol[];
}
