import { ApiProperty } from "@nestjs/swagger";
import { UsuarioResponseDto } from "./usuario.response.dto";


export class MetaResponseDto{
    @ApiProperty({type:Number})
    totalItems: number;

    @ApiProperty({type:Number})
    
    @ApiProperty({type:Number})
    itemCount: number;
    
    @ApiProperty({type:Number})
    itemsPerPage: number;
    
    @ApiProperty({type:Number})
    totalPages: number;
    
    @ApiProperty({type:Number})
    currentPage: number;
}

export class PaginateUsuarioResponseDto{
    @ApiProperty({type:[UsuarioResponseDto]})
    items:UsuarioResponseDto[];

    @ApiProperty({type:MetaResponseDto})
    meta:MetaResponseDto;
}