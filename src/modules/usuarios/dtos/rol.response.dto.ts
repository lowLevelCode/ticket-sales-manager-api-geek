import { ApiProperty } from "@nestjs/swagger";

export class RolResponseDto {
    @ApiProperty({type:String})
    id:string;    
    
    @ApiProperty({type:String})
    type:string;
}