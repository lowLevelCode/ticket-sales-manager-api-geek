import { ApiProperty } from "@nestjs/swagger";
import { 
  IsOptional, IsString, MaxLength,
  IsEmail, IsArray, IsNotEmpty, IsBoolean } from "class-validator";
import { Rol } from "./create-usuario.dto";


export class UpdateUsuarioDto {

  @ApiProperty({description:"Nombre del usuario",example:"juanito"})
  @IsNotEmpty()
  @IsString()
  @MaxLength(255)
  nombre: string;
  
  @ApiProperty({description:"Apellidos del usuario",example:"perez"})
  @IsNotEmpty()
  @IsString()
  @MaxLength(255)
  apellidos: string;
  
  @ApiProperty({description:"Correo del usuario",example:"example@gmail.com"})
  @IsNotEmpty()
  @IsEmail()
  email: string;  

  @ApiProperty({description:"Roles del usuario",example:[{id:'uuid'}]})
  @IsNotEmpty()
  @IsArray()
  roles:Rol[];
  
}
