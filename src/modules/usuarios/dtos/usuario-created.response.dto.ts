import { ApiProperty, OmitType } from "@nestjs/swagger";
import { UsuarioResponseDto } from "./usuario.response.dto";

export class RolUsuarioCreateDto{
    @ApiProperty({type:String})
    id:string;
}

export class UsuarioCreatedResponseDto extends OmitType(UsuarioResponseDto, ['roles'] as const){    

    @ApiProperty({type:[RolUsuarioCreateDto]})
    roles:RolUsuarioCreateDto[];

    @ApiProperty({type:Date})
    createdAt:Date;

    @ApiProperty({type:Date})
    updatedAt: Date;
    
    @ApiProperty({type:Boolean})
    status: boolean;
    
    @ApiProperty({type:Number})
    version: number;
}