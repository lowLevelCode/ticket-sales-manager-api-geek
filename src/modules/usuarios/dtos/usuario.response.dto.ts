import { ApiProperty } from "@nestjs/swagger";
import { RolResponseDto } from "./rol.response.dto";

export class UsuarioResponseDto{
    @ApiProperty({type:String})
    id:string;

    @ApiProperty({type:String})
    nombre:string;

    @ApiProperty({type:String})
    apellidos:string;

    @ApiProperty({type:String})
    email:string;

    @ApiProperty({type:[RolResponseDto]})
    roles:RolResponseDto[];
}