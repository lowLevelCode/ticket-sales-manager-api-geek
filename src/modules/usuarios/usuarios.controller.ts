import { Body, Controller, Delete, Get, Param, Post, Put, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiBadRequestResponse, ApiBody, ApiCreatedResponse, ApiInternalServerErrorResponse, ApiNotFoundResponse, ApiOkResponse, ApiOperation, ApiParam, ApiResponse, ApiResponseProperty, ApiTags } from '@nestjs/swagger';
import { FilterKeyDto, FilterKeyRolDto, FilterRolDto } from '../shared/dtos/filter.dto';
import { PaginationDto } from '../shared/dtos/pagination.dto';
import { BadRequestResponseNest, InternalServerRequestResponseNest, NotFoundRequestResponseNest } from '../shared/dtos/response-message/response.nest';
import { CreateUsuarioDto } from './dtos/create-usuario.dto';
import { PaginateUsuarioResponseDto } from './dtos/paginate-usuario.response.dto';
import { UpdateUsuarioDto } from './dtos/update-usuario.dto';
import { UsuarioCreatedResponseDto } from './dtos/usuario-created.response.dto';
import { UsuariosService } from './usuarios.service';

@ApiTags('usuarios')
@Controller('usuarios')
export class UsuariosController {
     
    constructor(private readonly _usuariosService:UsuariosService){}

    @ApiOperation({description:"Filtra usuarios por palabra clave y tipo de rol", summary:"Filtra por palabra clave y tipo de rol."})
    @ApiOkResponse({description:"Retorna un objeto paginado", type:PaginateUsuarioResponseDto})
    @ApiInternalServerErrorResponse({description:"Cualquier error interno del servidor", type:InternalServerRequestResponseNest})
    @Get('filter-by-key-rol')
    async getPaginateUsuariosFilterKeyNRol(@Query() paginationDto:PaginationDto, @Query() filterDto:FilterKeyRolDto) {             
        return this._usuariosService.getPaginateUsuariosFilterByKeyNRol(paginationDto,filterDto);
    }

    @ApiOperation({description:"Filtra usuarios por palabra clave", summary:"Filtra por palabra clave."})    
    @ApiOkResponse({description:"Retorna un objeto paginado", type:PaginateUsuarioResponseDto})
    @ApiInternalServerErrorResponse({description:"Cualquier error interno del servidor", type:InternalServerRequestResponseNest})
    @Get('filter-by-key')
    async getPaginateUsuariosFilterKey(@Query() paginationDto:PaginationDto, @Query() filter:FilterKeyDto) {        
        return this._usuariosService.getPaginateUsuariosFilterByKey(paginationDto,filter);
    }

    @ApiOperation({description:"Filtra usuarios por rol", summary:"Filtra por rol."})    
    @ApiOkResponse({description:"Retorna un objeto paginado", type:PaginateUsuarioResponseDto})
    @ApiInternalServerErrorResponse({description:"Cualquier error interno del servidor", type:InternalServerRequestResponseNest})
    @Get('filter-by-rol')
    async getPaginateUsuariosFilterRol(@Query() paginationDto:PaginationDto, @Query() filter:FilterRolDto) {        
        return this._usuariosService.getPaginateUsuariosFilterByRol(paginationDto,filter);
    }

    @ApiOperation({description:"Retorna usuarios de forma paginada", summary:"Obtiene Usuarios de forma paginada."})    
    @ApiOkResponse({description:"Retorna un objeto paginado", type:PaginateUsuarioResponseDto})
    @ApiInternalServerErrorResponse({description:"Cualquier error interno del servidor", type:InternalServerRequestResponseNest})
    @Get()
    async getPaginateUsuarios(@Query() paginationDto:PaginationDto) {        
        return this._usuariosService.getPaginateUsuarios(paginationDto);
    }

    @ApiOperation({description:"Registro para un nuevo usuario.", summary:"Registra un nuevo usuario."})
    @ApiBody({description:"Recibe un dto para crear un usuario", type:CreateUsuarioDto})
    @ApiCreatedResponse({description:"Retorna el usuario registrado",type:UsuarioCreatedResponseDto})
    @ApiBadRequestResponse({description:"Si el email a registrar ya existe", type:BadRequestResponseNest})
    @ApiInternalServerErrorResponse({description:"Cualquier error interno del servidor", type:InternalServerRequestResponseNest})
    @Post('register')
    register(@Body() usuario:CreateUsuarioDto) {
        return this._usuariosService.register(usuario);        
    }

    @ApiOperation({description:"Actualiza un usuario mediante su id.", summary:"Actualiza un usuario por uuid."})
    @ApiParam({name:'id', type:String, description:'uuid de usuario a actualizar'})
    @ApiBody({description:"Recibe un dto para actualizar usuario", type:UpdateUsuarioDto})
    @ApiOkResponse({description:"Retorna el usuario actualizado", type:UsuarioCreatedResponseDto})
    @ApiBadRequestResponse({description:"Si el id esta vacio, nulo o indefinido, o el email a actualizar ya esta asignado", type:BadRequestResponseNest})
    @ApiNotFoundResponse({description:"Si no existe usuario con el id a buscar o si es status = false", type:NotFoundRequestResponseNest})
    @ApiInternalServerErrorResponse({description:"Cualquier error interno del servidor", type:InternalServerRequestResponseNest})
    @Put('update/:id')
    async updateOne(@Param('id') id:string | number, @Body() usuario:UpdateUsuarioDto) {
        return this._usuariosService.updateOne(id,usuario);
    }

    @ApiOperation({description:"Elimina un usuario de forma logica mediante su id.", summary:"Elimina un usuario por uuid."})
    @ApiOkResponse({description:"Retorna el mensaje: 'Usuario {id} eliminado correctamente.'"})
    @ApiBadRequestResponse({description:"Si el id esta vacio, nulo o indefinido.", type:BadRequestResponseNest})
    @ApiNotFoundResponse({description:"Si no existe usuario con el id a buscar o si es status = false", type:NotFoundRequestResponseNest})
    @ApiInternalServerErrorResponse({description:"Cualquier error interno del servidor", type:InternalServerRequestResponseNest})
    @ApiParam({name:'id', type:String, description:'uuid de usuario a eliminar'})
    @Delete('delete/:id')
    async deleteOne(@Param('id') id:string | number) {        
        return this._usuariosService.deleteOne(id);
    }
}
