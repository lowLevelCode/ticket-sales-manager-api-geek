import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { UsuarioRepository } from 'src/database/repositories/usuario.repository';
import { CreateUsuarioDto } from './dtos/create-usuario.dto';
import { UpdateUsuarioDto } from './dtos/update-usuario.dto';
import { Usuarios } from "../../database/entities/usuarios.entity";
import { IPaginationOptions } from 'nestjs-typeorm-paginate/dist/interfaces';
import { FilterKeyDto, FilterKeyRolDto, FilterRolDto } from '../shared/dtos/filter.dto';

@Injectable()
export class UsuariosService {

    constructor(private readonly _usuarioRepo:UsuarioRepository){}
    
    async getPaginateUsuariosFilterByRol(options:IPaginationOptions, filter:FilterRolDto) {        
        return this._usuarioRepo.paginateFilterByRol(options,filter);
    }

    async getPaginateUsuariosFilterByKey(options:IPaginationOptions, filter:FilterKeyDto) {        
        return this._usuarioRepo.paginateFilterByKey(options,filter);
    }

    async getPaginateUsuariosFilterByKeyNRol(options:IPaginationOptions, filter:FilterKeyRolDto) {        
        return this._usuarioRepo.paginateFilterByKeyNRol(options,filter);
    }

    async getPaginateUsuarios(options:IPaginationOptions) {
        const pagination:any = await this._usuarioRepo.paginate(options);
        pagination.items.forEach(e => { delete e["password"];});
        return pagination
    }

    async register(usuarioDto:CreateUsuarioDto) {        
        const userExist = await this._usuarioRepo.findOne({ email: usuarioDto.email });
        if (userExist)
            throw new BadRequestException('User already registered with email');
        
        const newUsuario = this._usuarioRepo.create(usuarioDto);
        const user = await this._usuarioRepo.save(newUsuario);

        delete user.password;
        return user;
    }

    async updateOne(id:string | number, usuarioDto:UpdateUsuarioDto) {

        if(!id)
            throw new BadRequestException('id is empty, null or undefined');

        const userEmailExist = await this._usuarioRepo.findOne({ email: usuarioDto.email });
        if (userEmailExist && userEmailExist.id != id)
            throw new BadRequestException('User already registered with email');

        let userExist:Usuarios = await this._usuarioRepo.findOne(id); 
        if(!userExist || !userExist.status) 
            throw new NotFoundException();
        
        Object.assign(userExist, usuarioDto);   
        
        const user = await this._usuarioRepo.save(userExist);

        delete user.password;
        return user;
    }

    async deleteOne(id:string | number){
        
        if(!id)
            throw new BadRequestException('id is empty, null or undefined');

        let userExist:Usuarios = await this._usuarioRepo.findOne(id); 

        if(!userExist || !userExist.status)
            throw new NotFoundException();
        
        userExist.status = false;
        await this._usuarioRepo.save(userExist);

        return "Usuario " + id + " eliminado correctamente.";
    }
}
