import { Type } from "class-transformer";
import { IsArray, IsNotEmpty, IsNotEmptyObject, IsUUID, ValidateNested } from "class-validator";

export class ClienteId {

    @IsNotEmpty()
    @IsUUID()
    id:string;
}

export class UsuarioId {

    @IsNotEmpty()
    @IsUUID()
    id:string;
}

export class FuncionesId{

    @IsNotEmpty()
    @IsUUID()
    id:string;
}

export class CreateVentaDto {

    @IsNotEmptyObject()
    @ValidateNested()
    @Type(()=> ClienteId)
    cliente:ClienteId;

    @IsNotEmptyObject()
    @ValidateNested()
    @Type(()=> UsuarioId)
    usuario:UsuarioId;

    @IsArray()
    @ValidateNested()
    @Type(()=> FuncionesId)
    funciones:FuncionesId[];
}