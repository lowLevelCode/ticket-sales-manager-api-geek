import { Controller, Get, Query, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { VentasService } from './ventas.service';
import { CreateVentaDto } from './dtos/create-venta.dt';
import { UpdateVentaDto } from './dtos/update-venta.dto';

@Controller('ventas')
export class VentasController {
       
    constructor(private readonly _ventaService:VentasService){}

    @Get()
    async getUsuarios(){
        return this._ventaService.getUsuarios();
    }

    @Post('register')
    register(@Body() ventaDto:CreateVentaDto) {  
        console.log("la venta",ventaDto);
        return this._ventaService.register(ventaDto);        
    }

    @Put('update/:id')
    async updateOne(@Param('id') id:string | number, @Body() ventaDto:UpdateVentaDto){
        return this._ventaService.updateOne(id,ventaDto);
    }

    @Delete('delete/:id')
    async deleteOne(@Param('id)') id:string | number){
        return this._ventaService.deleteOne(id);
    }
}
