import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VentasRepository } from 'src/database/repositories/ventas.repository';
import { VentasController } from './ventas.controller';
import { VentasService } from './ventas.service';

@Module({
  imports:[TypeOrmModule.forFeature([VentasRepository])],
  controllers: [VentasController],
  providers: [VentasService]
})
export class VentasModule {}
