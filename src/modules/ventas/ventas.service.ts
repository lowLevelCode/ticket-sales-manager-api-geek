import { Injectable } from '@nestjs/common';
import { VentasRepository } from 'src/database/repositories/ventas.repository';
import { CreateVentaDto } from './dtos/create-venta.dt';
import { UpdateVentaDto } from './dtos/update-venta.dto';

@Injectable()
export class VentasService {
    
    constructor(private readonly _ventaRepo:VentasRepository){}

    async getUsuarios(){
        return this._ventaRepo.find({relations:["cliente"]});
    }

    async register(ventaDto:CreateVentaDto) {        
        return this._ventaRepo.save(ventaDto);
    }

    async updateOne(id:string | number, ventaDto:UpdateVentaDto){
        return "venta actualizado";
    }

    async deleteOne(id:string | number){
        return "venta eliminado";
    }
}
